## 1.0.1 (15-03-2019)

### Fixed (2 changement)

* Mise en place du fichier humans.txt
* Fait évoluer le formalisme du fichier CHANGELOG (Issue #4)

## 1.0.0 (18-03-2018)

### Release

* Mise en place de mon site CV ZIEGLER Morgan => https://zieglermorgan.gitlab.io
