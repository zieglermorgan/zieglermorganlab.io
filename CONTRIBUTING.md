# Comment déclarer un ticket ?

Vous avez trouvé un bug, vous souhaitez faire remonter une optimisation ou une évolution, ne cherchez plus, rendez-vous dans la section [Issues][issues-page].
Une issue peut revêtir différents labels mais qu'on peut catégoriser de la sorte :

* Priorité : ~Bug, ~Evolution, ~Optimisation
* Langage : ~html, ~css, ~javascript, ~highcharts, ~yaml
* Plateforme : ~gitlab, ~navigateur, ~firefox, ~chrome, ~safari, ~edge, ~"internet explorer"

Afin qu'une issue soit le plus exhaustif possible, elle devrait _toujours_ renseigner au moins un label de chaque type. La liste des labels ainsi que leur signification se trouve dans la section [Labels][labels-page].

[issues-page]: https://gitlab.com/zieglermorgan/zieglermorgan.gitlab.io/issues
[labels-page]: https://gitlab.com/zieglermorgan/zieglermorgan.gitlab.io/labels

## Les labels Priorité

Ce type de label est très important. Il définit quelle sorte d'issue ce sera. En général, chaque issue devrait en déclarer au moins un.

La liste des labels Priorité actuels est :

* ~Bug : Pour un bug rencontré dans l'application
* ~Evolution : Pour une évolution du site (design, nouvelle page, etc)
* ~Optimisation : Pour une optimisation au niveau du code, une nouvelle façon de penser un algorithme

Les labels Priorité sont toujours écrit avec la première lettre en majuscule ainsi ils sont montrés en premier dans une issue.

## Les labels Langage

Ce type de label est important. Il définit sur quel langage porte l'issue. En général, chaque issue devrait en déclarer au moins un.

La liste des labels Langage actuels est :

* ~html : Pour le langage HTML
* ~css : Pour le langage CSS
* ~javascript : Pour le langage JavaScript
* ~highcharts : Pour le Framework Highcharts
* ~yaml : Pour le langage Yaml

Les labels Langage sont toujours écrit en minuscule et ont pour couleur le gris.

## Les labels Plateforme

Ce type de label est important. Il définit sur quel plateforme porte l'issue. En général, chaque issue devrait en déclarer au moins un.

La liste des labels Plateforme actuels est :

* ~gitlab : Thématique générale qui porte sur GitLab
* ~navigateur : Thématique générale se rapportant à tous les navigateurs
* ~desktop : Thématique générale se rapportant aux PC
* ~mobile : Thématique générale se rapportant aux mobiles
* ~firefox : Pour Firefox
* ~chrome : Pour Chrome
* ~safari : Pour Safari
* ~edge : Pour Edge (Merci de préciser la version)
* ~"internet explorer" : Pour l'antique Internet Explorer
* ~android : Pour Android
* ~ios : Pour IOS

Les labels Plateforme sont toujours écrit en minuscule et revêtent deux couleurs. Bleu foncé pour des thématiques générales, bleu clair pour des thématiques plus précises.
