# CV ZIEGLER Morgan

[https://zieglermorgan.gitlab.io](https://zieglermorgan.gitlab.io)

## Le mot de l'auteur

Bienvenue sur mon CV.

Ce site a été réalisé en respectant les standards W3C et d'accessibilité (AccessiWeb 2.2) et s'appuie sur les guidelines Google [https://developers.google.com/web/fundamentals](https://developers.google.com/web/fundamentals).

Afin de parfaire le site, celui-ci a été soumis à toute une batterie de tests au travers d'outils comme :

* [DareBoost](https://www.dareboost.com)
* [Html5 Outliner](https://gsnedders.html5.org/outliner/)
* [Lighthouse](https://developers.google.com/web/tools/lighthouse/)
* [PageSpeed Tools](https://developers.google.com/speed/pagespeed/insights/)
* [Sonarwhal](https://sonarwhal.com)
* [TinyPng](https://tinypng.com)
* [Wave](http://wave.webaim.org)
* [WebPageTest](https://www.webpagetest.org/)
* [Website Speed Test](https://webspeedtest.cloudinary.com)
* [YellowLab](http://yellowlab.tools/)

Dans la mesure du possible (Exceptions faites des issues de configuration serveur où à ce jour GitLab Pages ne le permet pas), le site s'efforce de toutes les valider.

## Développement

### Extensions VS Code

* [EditorConfig for VS Code](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig)
* [Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)

### Afficher le site

#### Générer les dépendances externes du site

Depuis la racine du site, faire :

* cd src/
* yarn install --ignore-optional
* Lancez index.html situé dans le dossier www

#### Déployer le site

Pour valider le process de déploiement, depuis la racine du site :

* yarn install --ignore-optional
* yarn gulp
* Le site est généré dans le dossier public

#### Optimiser les images du site

Depuis la racine du site :

* yarn install --ignore-optional
* yarn gulp-dev
* /!\ Les images seront remplacées à la source
