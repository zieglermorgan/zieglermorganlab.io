const gulp = require('gulp');
const imagemin = require('gulp-imagemin');

const destFolder = 'src/www/design/img/';

gulp.task('jpg', () => {
  return gulp
    .src('src/www/design/img/**/*.jpg')
    .pipe(imagemin(imagemin.jpegtran({ progressive: true }), { verbose: true }))
    .pipe(gulp.dest(destFolder));
});

gulp.task('png', () => {
  return gulp
    .src('src/www/design/img/**/*.png')
    .pipe(
      imagemin(imagemin.optipng({ optimizationLevel: 7 }), { verbose: true })
    )
    .pipe(gulp.dest(destFolder));
});

gulp.task('svg', () => {
  return gulp
    .src('src/www/design/img/**/*.svg')
    .pipe(
      imagemin(
        imagemin.svgo({
          plugins: [{ removeUselessStrokeAndFill: false }]
        }),
        { verbose: true }
      )
    )
    .pipe(gulp.dest(destFolder));
});

gulp.task('default', gulp.series(gulp.parallel('jpg', 'png', 'svg')));
