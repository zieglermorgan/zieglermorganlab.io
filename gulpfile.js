const del = require('del');
const gulp = require('gulp');
const csso = require('gulp-csso');
const htmlmin = require('gulp-htmlmin');
const uglifyes = require('gulp-uglifyes');
const rev = require('gulp-rev');
const revReplace = require('gulp-rev-replace');
const revDelOriginal = require('gulp-rev-delete-original');
const gzip = require('gulp-gzip');
const brotli = require('gulp-brotli');

// Répertoire de sortie (dest)
const destFolder = 'public';

gulp.task('clean', () => {
  return del('public/**/*');
});

gulp.task('copy', () => {
  return gulp
    .src(
      [
        'src/www/**/*',
        '!src/www/lib/**/*',
        'src/www/lib/highcharts/highcharts.src.js',
        'src/www/lib/lazysizes/lazysizes.js',
        'src/www/lib/normalize.css/normalize.css',
        'src/www/lib/npm-font-open-sans/fonts/Bold/OpenSans-Bold.woff',
        'src/www/lib/npm-font-open-sans/fonts/Bold/OpenSans-Bold.woff2',
        'src/www/lib/source-sans-pro/WOFF/OTF/SourceSansPro-Regular.otf.woff',
        'src/www/lib/source-sans-pro/WOFF2/TTF/SourceSansPro-Regular.ttf.woff2',
        'src/www/lib/source-sans-pro/WOFF/OTF/SourceSansPro-Bold.otf.woff',
        'src/www/lib/source-sans-pro/WOFF2/TTF/SourceSansPro-Bold.ttf.woff2'
      ],
      { base: 'src/www/' }
    )
    .pipe(gulp.dest(destFolder));
});

gulp.task('html', () => {
  return gulp
    .src('public/**/*.html')
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest(destFolder));
});

gulp.task('css', () => {
  return gulp
    .src('public/**/*.css')
    .pipe(csso())
    .pipe(gulp.dest(destFolder));
});

gulp.task('js', () => {
  return gulp
    .src('public/**/*.js')
    .pipe(uglifyes())
    .pipe(gulp.dest(destFolder));
});

gulp.task('rev', () => {
  return gulp
    .src(['public/**/*.{css,js,jpg,png,svg}'])
    .pipe(rev())
    .pipe(revDelOriginal())
    .pipe(gulp.dest(destFolder))
    .pipe(rev.manifest())
    .pipe(gulp.dest(destFolder));
});

gulp.task('revReplace', () => {
  let manifestPath = gulp.src('public/rev-manifest.json');

  return gulp
    .src('public/**/*.html')
    .pipe(revReplace({ manifest: manifestPath }))
    .pipe(gulp.dest(destFolder));
});

gulp.task('cleanManifest', () => {
  return del('public/rev-manifest.json');
});

gulp.task('gzip', () => {
  return gulp
    .src(['public/**/*.{html,css,js,svg,xml,webmanifest}'])
    .pipe(gzip({ gzipOptions: { level: 9 } }))
    .pipe(gulp.dest(destFolder));
});

gulp.task('brotli', () => {
  return gulp
    .src(['public/**/*.{html,css,js,svg,xml,webmanifest}'])
    .pipe(brotli.compress({ mode: 0, quality: 11 }))
    .pipe(gulp.dest(destFolder));
});

gulp.task(
  'default',
  gulp.series(
    'clean',
    'copy',
    gulp.parallel('html', 'css', 'js'),
    'rev',
    'revReplace',
    'cleanManifest',
    gulp.parallel('gzip', 'brotli')
  )
);
