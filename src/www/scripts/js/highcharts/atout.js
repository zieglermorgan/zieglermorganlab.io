/*  Définit les données du graphique  */
function defineData(category, ratio, colorIntern, colorExtern) {
  return {
    y: ratio,
    color: colorIntern,
    drilldown: {
      categories: category,
      y: ratio,
      color: colorExtern
    }
  };
}

let colorsAtout = [
  '#F1C2BA',
  '#A5A1A2',
  '#D3D4D6',
  '#C0C1C5',
  '#EF5F5E',
  '#000000',
  '#949599',
  '#58585A'
];

let data1 = defineData('Disponible', 23, colorsAtout[1], colorsAtout[5]);
let data2 = defineData('Autodidacte', 31, colorsAtout[0], colorsAtout[4]);
let data3 = defineData('Ingénieux', 23, colorsAtout[2], colorsAtout[6]);
let data4 = defineData('Persévérant', 23, colorsAtout[3], colorsAtout[7]);
let data = [data1, data2, data3, data4];

let dataIntern = [];
let dataExtern = [];
for (let i = 0, j = data.length; i < j; i++) {
  // Alimente les données du cercle interne
  dataIntern.push({
    y: data[i].y,
    color: data[i].color
  });

  // Alimente les données du cercle externe
  dataExtern.push({
    name: data[i].drilldown.categories,
    y: data[i].drilldown.y,
    color: data[i].drilldown.color
  });
}

Highcharts.chart('atout', {
  chart: {
    type: 'pie',
    style: {
      fontFamily: 'Source Sans Pro'
    },
    printHeight: 300,
    printWidth: 410
  },
  title: {
    text: null
  },
  series: [
    {
      data: dataIntern,
      size: '55%',
      dataLabels: {
        enabled: false
      }
    },
    {
      data: dataExtern,
      size: '80%',
      innerSize: '80%',
      dataLabels: {
        distance: 30,
        style: {
          color: '#333333',
          fontSize: '1.5rem',
          fontWeight: 'normal'
        }
      }
    }
  ],
  plotOptions: {
    pie: {
      shadow: false,
      center: ['50%', '50%'],
      states: {
        hover: {
          enabled: false
        }
      }
    }
  },
  tooltip: {
    enabled: false
  },
  credits: {
    enabled: false
  },
  responsive: {
    rules: [
      {
        condition: {
          maxWidth: 400
        },
        chartOptions: {
          series: [
            {
              size: '65%'
            },
            {
              size: '95%',
              innerSize: '80%',
              dataLabels: {
                distance: -90
              }
            }
          ]
        }
      },
      {
        condition: {
          maxWidth: 320
        },
        chartOptions: {
          series: [
            {
              size: '65%'
            },
            {
              size: '95%',
              innerSize: '80%',
              dataLabels: {
                distance: -75
              }
            }
          ]
        }
      }
    ]
  }
});
