let colorsComp = { 4: '#FD5959', 3: '#A5A5A5', 2: '#333333' };

Highcharts.chart('competence', {
  chart: {
    type: 'bar',
    style: {
      fontFamily: 'Source Sans Pro'
    },
    printHeight: 300,
    printWidth: 460
  },
  title: {
    text: null
  },
  series: [
    {
      data: [4, 4, 3, 4, 2, 3, 2, 2, 4, 3, 3, 4],
      colors: [
        colorsComp[4],
        colorsComp[4],
        colorsComp[3],
        colorsComp[4],
        colorsComp[2],
        colorsComp[3],
        colorsComp[2],
        colorsComp[2],
        colorsComp[4],
        colorsComp[3],
        colorsComp[3],
        colorsComp[4]
      ]
    }
  ],
  xAxis: {
    categories: [
      'Accessibilité Web',
      'Merise',
      'UML',
      'HTML/CSS',
      'JavaScript',
      'Java',
      'C#',
      'PHP',
      'SQL (PL/SQL, T-SQL)',
      'Oracle/SQL Server',
      'PostgreSQL/MySQL',
      'Git'
    ],
    labels: {
      y: 4,
      style: {
        color: '#333333',
        fontSize: '1.5rem',
        fontWeight: 'normal',
        whiteSpace: 'nowrap'
      }
    },
    lineWidth: 0,
    tickWidth: 0
  },
  yAxis: {
    title: {
      text: null
    },
    labels: {
      enabled: false
    },
    endOnTick: false,
    maxPadding: 0.1,
    gridLineWidth: 0
  },
  plotOptions: {
    series: {
      colorByPoint: true,
      pointWidth: 8
    }
  },
  tooltip: {
    enabled: false
  },
  legend: {
    enabled: false
  },
  credits: {
    enabled: false
  }
});
