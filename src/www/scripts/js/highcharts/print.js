// Permet de redimensionner le graphique pour l'impression (Uniquement pour IE et Edge)
if (
  navigator.userAgent.indexOf('Edge') !== -1 ||
  navigator.userAgent.indexOf('Trident') !== -1
) {
  let html = document.querySelector('html');

  // Ajout d'une classe CSS pour ne cibler que Edge < 16 ou IE
  if (navigator.userAgent.indexOf('Edge') !== -1) {
    let regex = /Edge\/([0-9]{2})/;
    if (regex.exec(navigator.userAgent)[1] < 16) {
      html.className = 'edge';
    }
  } else {
    html.className = 'ie';
  }

  // Redimensionne le graphique pour l'impression
  (function(hc) {
    hc.wrap(hc.Chart.prototype, 'initReflow', function(proceed) {
      let charts = this;
      proceed.call(this);
      hc.addEvent(window, 'beforeprint', function() {
        charts._screenMediaWidth = charts.chartWidth;
        charts._screenMediaHeight = charts.chartHeight;
        charts.setSize(
          charts.options.chart.printWidth,
          charts.options.chart.printHeight,
          false
        );
      });
      hc.addEvent(window, 'afterprint', function() {
        charts.setSize(
          charts._screenMediaWidth,
          charts._screenMediaHeight,
          false
        );
      });
    });
  })(Highcharts);
}
